import { Box, Container } from "@mui/material";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";

import { NextPage } from "next";

const Home: NextPage = () => {
  const { t } = useTranslation("home");

  return (
    <Container maxWidth="lg">
      <Box
        sx={{
          mb: 18,
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          mt: 25,
        }}
      >
        this is my Home page
      </Box>
    </Container>
  );
};
export default Home;
export const getStaticProps = async ({ locale }: { locale: string }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common", "home"])),
    },
  };
};
