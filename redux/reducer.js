import * as types from "./actionTypes";

const initialState = {
  darkTheme: true,
};

const preferences = (state = initialState, action) => {
  switch (action.type) {
    case types.TOGGLE_DARKTHEME:
      return {
        ...state,
        darkTheme: !state.darkTheme,
      };
    default:
      return state;
  }
};


export default preferences;
