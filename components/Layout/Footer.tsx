import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import { Box, Typography, Fab } from "@mui/material";
import { useTranslation } from "next-i18next";
import { ScrollTop } from "./ScrollTop";

export default function Footer({ props }: { props: any }) {
  const { t } = useTranslation("home");

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        maxHeight: "120vh",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box
        component="footer"
        sx={{
          py: 3,
          px: 2,
          mt: 15,
          background: "transparent",
        }}
      >
        <Typography variant="body2" fontSize={18}>
          {t("Copyright")}
          <strong>{t("fatema")}</strong>
          {new Date().getFullYear()}
        </Typography>
      </Box>
      <ScrollTop {...props}>
        <Fab size="small" aria-label="scroll back to top">
          <KeyboardArrowUpIcon />
        </Fab>
      </ScrollTop>
    </Box>
  );
}
