import axios from "axios";

export const axiosClient = axios.create({
  baseURL: process.env.MY_DOMAIN,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export const getAllPosts = () => {
  return axiosClient.get("/posts");
};
export const getPost = (id: string) => {
  return axiosClient.get(`/posts/${id}`);
};
