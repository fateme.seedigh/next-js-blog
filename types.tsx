export interface Props {
  window?: () => Window;
}
export type Anchor = "top" | "left" | "bottom" | "right";

export interface allPostsType {
  allPosts: {
    map: any;
    id: number;
    title: string;
    body: string;
    tags: [string];
  };
}
export interface data {
  map: any;
  id: string;

  title: string;
}
export interface postsMapType {
  id: number;
  title: string;
  body: string;
  tags: [string];
}
export interface postsType {
  title: string;
  body: string;
  tags: [string];
  id:number
}
export interface TemporaryDrawerType {
  alldata: data[] |undefined;
  setSearch: any;
}

export interface postType {
  post: {
    id: number;
    title: string;
    body: string;
    tags: [string];
  };
}