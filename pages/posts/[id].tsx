import { Box, Container, Typography } from "@mui/material";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import { NextPage } from "next";
import { getAllPosts, getPost } from "../../lib/blogApi";
import { postType } from "../../types";
import QueryBuilderIcon from "@mui/icons-material/QueryBuilder";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import Image from "next/image";
import { useRouter } from "next/router";

const Post: NextPage<postType> = ({ post }) => {
  const { t } = useTranslation("home");
  const router = useRouter();

  return (
    <Container maxWidth="lg">
      <Typography variant="h2" textAlign="center" pt={25}>
        {post.title}
      </Typography>
      <Box
        display="flex"
        justifyContent="flex-start"
        alignItems="flex-start"
        mx={10}
        my={5}
      >
        <Typography px={2} fontSize={14}>
          {t("writer")}
        </Typography>
        <CalendarMonthIcon fontSize="small" />
        <Typography px={2} fontSize={14}>
          {t("date-post")}
        </Typography>
        <Typography px={2}>—</Typography>
        <QueryBuilderIcon fontSize="small" />
        <Typography px={1} fontSize={14}>
          {t("min-read")}
        </Typography>
      </Box>
 
        {router.locale === "en" ? (
          <Image src="/images/blog1.jpg" height={420} width={1150} alt="blog" />
        ) : (
          <Image src="/images/images.png" height={420} width={1150} alt="blog" />
        )}
        <Typography my={4}>
          {post.body}
        </Typography>
    </Container>
  );
};
export default Post;

export const getStaticPaths = async (ctx: { locales: any }) => {
  const { locales } = ctx;
  const res = await getAllPosts();
  const allPosts = res.data.posts;
  const path = (locale: string) =>
    allPosts.map((post: { id: any }) => ({
      params: { id: post.id.toString() },
      locale,
    }));
  const paths = locales?.map((locale: string) => path(locale)).flat() || [];

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async (x: {
  params: { id: string };
  locale: string;
}) => {
  const { id } = x.params;
  const locales = x.locale;
  const res = await getPost(id);
  const post = res.data;
  return {
    props: {
      post,
      ...(await serverSideTranslations(locales, ["common", "home"])),
    },
  };
};
