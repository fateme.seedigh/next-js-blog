import * as React from "react";
import { TextField, Typography, Button, Box, Grid } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import CloseIcon from "@mui/icons-material/Close";
import Drawer from "@mui/material/Drawer";
import styles from "./custom.module.css";
import { Anchor, TemporaryDrawerType } from "../../types";
import { useTranslation } from "next-i18next";
import type { NextPage } from "next";
import Link from "next/link";

export const TemporaryDrawer: NextPage<TemporaryDrawerType> = ({
  setSearch,
  alldata,
}) => {
  const { t } = useTranslation();

  const [open, setOpen] = React.useState(false);
  const handleDrawerToggle = () => {
    setOpen(!open);
  };
  const list = (anchor: Anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      py={8}
      px={5}
    >
      <Box display="flex" py={5}>
        <TextField
          variant="standard"
          color="info"
          placeholder={t("type-to-search-blog")}
          fullWidth
          focused
          onChange={(e) => setSearch(e.target.value)}
          sx={{ mt: 3 }}
        />
        <CloseIcon
          color="info"
          fontSize="large"
          sx={{ ml: 8, cursor: "pointer" }}
          onClick={handleDrawerToggle}
        />
      </Box>
      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="center"
        spacing={2}
      >
        {alldata?.map((item) => (
          <Grid item md={3} key={item.id}>
            <Link href={`/posts/${item.id}`}>
              <a>
                <div className={styles.tagStyle} onClick={handleDrawerToggle}>{item.title}</div>
              </a>
            </Link>
          </Grid>
        ))}
      </Grid>
      <Typography fontSize={18} mt={6}>
        {t("see-posts-by-tags")}
      </Typography>
      <Box display="flex" my={2}>
        <div className={styles.tagStyle}>one</div>
        <div className={styles.tagStyle}>oneone</div>
        <div className={styles.tagStyle}>oneoneone</div>
      </Box>
    </Box>
  );

  return (
    <>
      <Button onClick={handleDrawerToggle} color="secondary">
        <SearchIcon />
      </Button>
      <Drawer anchor="top" open={open} onClose={handleDrawerToggle}>
        {list("top")}
      </Drawer>
    </>
  );
};
export default TemporaryDrawer;
