import React from "react";
import Footer from "./Layout/Footer";
import { useSelector } from "react-redux";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { Navbar } from "./Layout/Navbar";
import { darkTheme, theme } from "../src/theme";


const MyThemeProvider = ({ children }) => {
  const mytheme = useSelector((state) => state.darkTheme);


  return (
    <ThemeProvider
      theme={mytheme ? theme : darkTheme}
      // theme={createTheme({
      //   direction: router.locale == "ar" ? "rtl" : "ltr",
      // })}
    >
      <CssBaseline />
      <Navbar />
      {children}
      <Footer props={undefined} />
    </ThemeProvider>
  );
};

export default MyThemeProvider;
