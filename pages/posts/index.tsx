import { Container, Typography, Box, Grid } from "@mui/material";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
import styles from "../../styles/Custom.module.css";
import { getAllPosts } from "../../lib/blogApi";
import { NextPage } from "next";
import { postsMapType, allPostsType } from "../../types";
import PostCard from "../../components/Blog/PostCard";

const Posts: NextPage<allPostsType> = ({ allPosts }) => {
  const { t } = useTranslation("home");

  return (
    <Container maxWidth="lg">
      <Box
        sx={{
          mb: 18,
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          mt: 25,
        }}
      >
        <mark className={styles.myMark}>
          <Typography
            variant="h1"
            fontSize={34}
            fontWeight="bold"
            className={styles.textTitle}
          >
            {t("all-posts")}
          </Typography>

          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 400 60"
            preserveAspectRatio="none"
            className={styles.mySvg}
          >
            <path d="m 3.518915,27.827324 c 55.429038,4.081 111.581115,5.822 167.117815,2.867 22.70911,-1.208 45.39827,-0.601 68.126,-0.778 28.38172,-0.223 56.76078,-1.024 85.13721,-1.33 24.17378,-0.261 48.4273,0.571 72.58114,0.571" />
          </svg>
          <svg
            className={styles.mySvg}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 400 60"
            preserveAspectRatio="none"
          >
            <path d="m 3.518915,27.827324 c 55.429038,4.081 111.581115,5.822 167.117815,2.867 22.70911,-1.208 45.39827,-0.601 68.126,-0.778 28.38172,-0.223 56.76078,-1.024 85.13721,-1.33 24.17378,-0.261 48.4273,0.571 72.58114,0.571" />
          </svg>
        </mark>
      </Box>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="flex-start"
        spacing={5}
      >
        {allPosts.map((index: postsMapType) => (
          <Grid item md={12} lg={6} key={index.id}>
            <PostCard title={index.title} body={index.body} tags={index.tags} id={index.id}/>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};
export default Posts;
export const getStaticProps = async ({ locale }: { locale: string }) => {
  const res = await getAllPosts();
  const allPosts = res.data.posts;
  return {
    props: {
      allPosts,
      ...(await serverSideTranslations(locale, ["common", "home"])),
      revalidate: 10,
    },
  };
};
