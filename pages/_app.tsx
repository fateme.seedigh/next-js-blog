import Head from "next/head";
import * as React from "react";
import "../styles/globals.css";
import type { NextPage } from "next";
import { Provider } from "react-redux";
import { wrapper } from "../redux/store";
import { appWithTranslation } from "next-i18next";
import MyThemeProvider from "../components/MyThemeProvider";
// import rtlPlugin from "stylis-plugin-rtl";

// Client-side cache, shared for the whole session of the user in the browser.
// const clientSideEmotionCache = createEmotionCache();

// const cacheRtl = createCache({
//   key: "muirtl",
//   stylisPlugins: [prefixer, rtlPlugin],
// });
interface AppProps {
  Component: any;
}
const MyApp: NextPage<AppProps> = ({ Component, ...rest }) => {
  const { store, props } = wrapper.useWrappedStore(rest);
  const { pageProps } = props;
  return (
    <Provider store={store}>
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <MyThemeProvider>
        <Component {...pageProps} />
      </MyThemeProvider>
    </Provider>
  );
};
export default appWithTranslation(MyApp);
