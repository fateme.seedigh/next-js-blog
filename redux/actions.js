import * as types from "./actionTypes";

export const toggleDarkTheme = () => ({
  type: types.TOGGLE_DARKTHEME,
});
