import { combineReducers } from "redux";
import preferences from "./reducer";

const rootReducer = combineReducers({
  data: preferences,
});

export default rootReducer;
