import { useState, useEffect } from "react";
import {
  Typography,
  Toolbar,
  IconButton,
  Drawer,
  Box,
  AppBar,
  Container,
} from "@mui/material";
import Image from "next/image";
import { data, Props } from "../../types";
import { useRouter } from "next/router";
import styles from "./navbar.module.css";
import MenuIcon from "@mui/icons-material/Menu";
import CloseIcon from "@mui/icons-material/Close";
import CustomizedSwitches from "../Customs/CustomizedSwitches";
import { useTranslation } from "next-i18next";
import Usedebounce from "../../hooks/Usedebounce";
import axios from "axios";
import TemporaryDrawer from "../Customs/TemporaryDrawer";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";

export const Navbar = (props: Props) => {
  const { t } = useTranslation();

  const { window } = props;
  const [mobileOpen, setMobileOpen] = useState(false);
  const router = useRouter();
  useEffect(() => {
    let dir = router.locale == "ar" ? "rtl" : "ltr";
    let lang = router.locale == "ar" ? "ar" : "en";
    (document as any).querySelector("html").setAttribute("dir", dir);
    (document as any).querySelector("html").setAttribute("lang", lang);
  }, [router.locale]);

  const handleLocaleChange = (value: string | undefined) => {
    if (value === "en") {
      router.push(router.route, router.asPath, {
        locale: "ar",
      });
    } else {
      router.push(router.route, router.asPath, {
        locale: "en",
      });
    }
  };
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <Box
      onClick={handleDrawerToggle}
      sx={{
        textAlign: "center",
        background: (theme) => theme.palette.primary.main,
      }}
      justifyContent="center"
      display="flex"
      pt={8}
    >
      <ul className={styles.ulMenuStyle}>
        <Link href="/">
          <li>{t("home")}</li>
        </Link>
        <Link href="posts">
          <li>{t("posts")}</li>
        </Link>
        <Link href="/">
          <li>{t("archive")}</li>
        </Link>
      </ul>
    </Box>
  );
  const container =
    window !== undefined ? () => window().document.body : undefined;
  const [search, setSearch] = useState<string | null>(null);
  const [alldata, setAllData] = useState<data[]>();
  const [loading, setLoading] = useState(false);
  const debouncedSearch = Usedebounce(search, 500);
  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      setAllData([]);
      axios
        .get(` https://dummyjson.com/posts/search?q=${debouncedSearch}`)
        .then((res) => setAllData(res.data.posts))
        // .then((res) => console.log(res.data.posts))
        .catch((err) => console.log(err));
      setLoading(false);
    }
    if (debouncedSearch) fetchData();
  }, [debouncedSearch]);
  return (
    <header>
      <div id="back-to-top-anchor" />
      <Box sx={{ display: "flex" }}>
        <AppBar
          component="nav"
          position="fixed"
          color="primary"
          className={styles.navabrStyle}
        >
          <Container>
            <Toolbar>
              <Typography
                variant="h6"
                component="div"
                sx={{
                  fontWeight: "bold",
                  flexGrow: 1,
                  display: { xs: "none", lg: "block" },
                }}
              >
                {t("blog")}
              </Typography>

              <Box width="100%">
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  edge="end"
                  onClick={handleDrawerToggle}
                  sx={{ mr: 2, display: { lg: "none" } }}
                >
                  {mobileOpen ? <CloseIcon /> : <MenuIcon />}
                </IconButton>
              </Box>
              <Box sx={{ display: { xs: "none", lg: "block" }, width: "70%" }}>
                <ul className={styles.ulStyle}>
                  <Link href="/">
                    <a>
                      <li>{t("home")}</li>
                    </a>
                  </Link>
                  <Link href="posts">
                    <a>
                      <li>{t("posts")}</li>
                    </a>
                  </Link>
                  <Link href="/">
                    <a>
                      <li>{t("archive")}</li>
                    </a>
                  </Link>
                </ul>
              </Box>
              <Box display="flex" alignItems="center">
                <div
                  className={styles.translateImage}
                  onClick={() => handleLocaleChange(router.locale)}
                >
                  {router.locale === "en" ? (
                    <Image
                      src="/icons/usa.png"
                      height={120}
                      width={120}
                      alt="usa"
                    />
                  ) : (
                    <Image
                      src="/icons/arab.png"
                      height={120}
                      width={120}
                      alt="arab"
                    />
                  )}
                </div>
                <CustomizedSwitches />

                <TemporaryDrawer setSearch={setSearch} alldata={alldata} />
              </Box>
            </Toolbar>
          </Container>
        </AppBar>
        <Box component="nav">
          <Drawer
            anchor="top"
            container={container}
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true,
            }}
            sx={{
              display: { xs: "block", lg: "none" },
              "& .MuiDrawer-paper": {
                boxSizing: "border-box",
              },
            }}
          >
            {drawer}
          </Drawer>
        </Box>
      </Box>
    </header>
  );
};
