import { Card, CardActions, CardContent, Typography, Box } from "@mui/material";
import styles from "./blog.module.css";
import bg from "../../public/images/how-to-upload.png";
import QueryBuilderIcon from "@mui/icons-material/QueryBuilder";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import { postsType } from "../../types";
import { NextPage } from "next";
import Link from "next/link";

const PostCard: NextPage<postsType> = ({ title, body, tags,id }) => {

  return (
    <article>
      <Link href={`posts/${id}`}>
      <a>
         <Card
        sx={{ maxWidth: "550px", background: "transparent", boxShadow: "none" }}
      >
        <div
          style={{
            zIndex: 1,
            marginBottom: "50px",
            position: "relative",
          }}
        >
          <div className={styles.block}>
            <Box
              sx={{
                marginLeft: "25px",
                backgroundImage: `url(${bg.src})`,
                height: "300px",
                width: { xs: 250, sm: 450 },
                backgroundPosition: "center ",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                position: "relative",
              }}
              className={styles.imageEffect}
            />
          </div>
        </div>

        <CardContent>
          <Box display="flex">
            <CalendarMonthIcon fontSize="small" />
            <Typography pl={1} pr={2} fontSize={14}>
              12 Aug, 2020
            </Typography>
            <Typography pr={2}>—</Typography>
            <QueryBuilderIcon fontSize="small" />
            <Typography pl={1} fontSize={14}>
              03 min read
            </Typography>
          </Box>
          <Typography
            gutterBottom
            variant="h2"
            component="div"
            fontSize={30}
            fontWeight="bold"
            py={2}
            sx={{
              "&:hover": { color: "#2667ff", transition: " .3s ease-in-out" }
            }}
          >
            {title}
          </Typography>
          <Typography variant="body2">{body}</Typography>
        </CardContent>
        <CardActions>
          <Typography fontSize={14}>by Tomas</Typography>
          {tags.map((item) => (
            <div className={styles.tagStyle} key={item}>
              {item}
            </div>
          ))}
        </CardActions>
      </Card>
      </a>
      </Link>
     
    </article>
  );
};
export default PostCard;
