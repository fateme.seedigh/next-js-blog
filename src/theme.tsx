import { createTheme } from "@mui/material/styles";
import { red } from "@mui/material/colors";

//#edf2fb,#e2eafc,#d7e3fc,#ccdbfd,#c1d3fe,#b6ccfe,#abc4ff
// Create a theme instance.


export const theme = createTheme({
  direction: "ltr",
  typography: {
    fontFamily: ["Crete Round", "sans-serif"].join(","),
  },
  palette: {
    primary: {
      main: "#d7e3fc",
    },
    info: {
      main: "#2667ff",
    },
    secondary: {
      main: "#000",
    },
    error: {
      main: red.A400,
    },
  },
});

export const darkTheme = createTheme({
  palette: {
    mode: "dark",
    background: {
      paper: "#222",
    },
    text: {
      primary: "#fff",
    },
  },
});
